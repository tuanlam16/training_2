export type categoryCar = {
    id: number;
    name: string;
    image: string;
};

export type garage = {
    id: number;
    name: string;
    image: string;
    categoryId: number[];
    rate: number;
    verified: boolean;
    address: string;
    numberBranches: number;
    distance: number;
};

export type Service = {
    id: number;
    name: string;
    price: number;
    createdAt: string;
    quantity: number;
    garage: garage;
};




import { View, ActivityIndicator, StyleSheet } from "react-native";
import { colors } from "../styles/colors";

export const Load = () => {
    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color={colors.yellowOrange} />
        </View>
    );
    };

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

import { makeAutoObservable, action, computed, observable } from "mobx";
import { categories, services } from "../constant/services";
import { categoryCar, Service } from "../types/serviceTypes";

class store {
    constructor() {
        makeAutoObservable(this);
}
@observable services : Service[] = services;
@observable selectedCategory : categoryCar = categories[0];
@observable decendingPrice : boolean = false;
@observable searchText : string = '';
@observable isRefresh : boolean = false;

@computed get getIsRefresh() {
    return this.isRefresh;
}

@computed get getServiceNearby() {
  return this.searchService(
    this.filterByCategory(
        this.services.slice().sort((a, b) => a.garage.distance - b.garage.distance)
    )
  )
}

@computed get getServiceNewest() {
    return this.searchService(
        this.filterByCategory(
            this.services.slice().sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
        )
    )
}

@computed get getServicesPopular() {
    return this.searchService(
        this.filterByCategory(
            this.services.slice().sort((a, b) => b.quantity - a.quantity)
        )
    )
}

@computed get getServicesByPrice() {
    return this.searchService(
        this.filterByCategory(
            this.services.slice().sort((a, b) => this.decendingPrice ? b.price - a.price : a.price - b.price)
        )
    )
}

@action setSearchText = (text : string) => {
    this.searchText = text;
}

@action searchService = (services: Service[]) => {
    return services
    .filter((service) =>
    service.name.toLowerCase().includes(this.searchText.toLowerCase()) ||
    service.garage.name.toLowerCase().includes(this.searchText.toLowerCase())
    );
}

@action filterByCategory = (services : Service[]) => {
    return services.filter((service) => service.garage.categoryId.includes(this.selectedCategory.id));
}

@action setSelectedCategory = (category : categoryCar) => {
    this.selectedCategory = category;
}

@action setDecendingPrice = (decendingPrice : boolean) => {
    this.decendingPrice = decendingPrice;
}

@action setIsRefresh = () => {
    this.isRefresh = true;
    //fetch data
    setTimeout(() => {
        this.isRefresh = false;
    }
    , 1000);
}

}

const serviceStore = new store();
export default serviceStore;
 


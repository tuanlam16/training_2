import { observer } from "mobx-react";
import { services } from "../../../constant/services";
import serviceStore from "../../../stores/serviceStore";
import { ContentContainer } from "./ContentComponent";

export const NewestTab = observer(() => {
   return (
        <ContentContainer service={serviceStore.getServiceNewest} />
    );
})

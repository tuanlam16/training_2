import React from "react";
import { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, RefreshControl } from "react-native";
import { Rating } from "react-native-ratings";
import { services } from "../../../constant/services";
import { colors } from "../../../styles/colors";
import { fontSizes } from "../../../styles/fonts";
import { Service } from "../../../types/serviceTypes";
import { width } from "../../../until/dimentions";

type Props = {
    service: Service[];
}
export const ContentContainer = (prop:Props) => {

    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = () => {
      setRefreshing(true);
      setTimeout(() => {
        setRefreshing(false);
      }, 1000);
    };

    const renderItem = ({item}: {item: Service}) => {
        const numberToCurrency = (number: number) => {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
        return (
            <TouchableOpacity style={styles.card}>
                <Image style={styles.image} source={item.garage.image} />
                <View style={styles.info}>
                    <View style={styles.infoTop}>
                        <View style={styles.infoTopLeft}>
                            <Image style={styles.verified} source={require('../../../assets/icons/verify.png')} />
                        <Text style={styles.nameGarage}>{item.garage.name}</Text>
                        </View>
                        <Text style={styles.distance}>{item.garage.distance} km</Text>
                    </View>
                    <Text style={styles.nameService}>{item.name}</Text>
                    <View style={styles.rating}>
                       <Rating
                        type="custom"
                        ratingColor={colors.yellowOrange}
                        ratingCount={5}
                        imageSize={15}
                        startingValue={item.garage.rate}
                        readonly
                        />
                    </View>
                    <View style={styles.infoBottom}>
                        <Text style={styles.price}>{numberToCurrency(item.price)} đ</Text>
                        <Text style={styles.address}>{item.garage.address}</Text>
                        <View style={styles.position}>
                            <Image style={styles.positionIcon} source={require('../../../assets/icons/location.png')} />
                            <Text style={styles.positionText}>{item.garage.numberBranches} chi nhánh khác</Text>
                            </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <View style={styles.container}>
            <FlatList
                data={prop.service}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl 
                    refreshing={refreshing} 
                    onRefresh={onRefresh} 
                    colors = {[colors.yellowOrange]}
                    />
                }
                ListEmptyComponent={() => (
                    <Text style={{
                        textAlign: 'center',
                        fontSize: fontSizes.text,
                        color: colors.black,
                        marginTop: 20,
                        fontFamily: "Montserrat-Regular"
                    }}>Không có dữ liệu</Text>
                )}
            />

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
    },
    card: {
        width: width - 40,
        height: 170,
        backgroundColor: colors.white,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 10,
        marginBottom: 20,
        padding: 10,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 10,
    },
    info: {
        width: '68%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    infoTop: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    infoTopLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    verified: {
        width: 15,
        height: 15,
        marginRight: 5,
    },
    
    nameGarage: {
        fontSize: fontSizes.text,
        fontFamily: 'Montserrat-Bold',
        color: colors.black,
    },
    distance: {
        fontSize: fontSizes.text,
        color: colors.black,
        fontFamily: 'Montserrat-Medium',
    },
    nameService: {
        fontSize: fontSizes.text,
        color: colors.black,
        fontFamily: 'Montserrat-Medium',
    },
    rating: {
        width: '100%',
        height: 15,
        alignItems: 'flex-start',
    },
    infoBottom: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    price: {
        fontSize: fontSizes.text,
        color: colors.yellowOrange,
        fontFamily: 'Montserrat-Medium',
        marginBottom: 5,
    },
    address: {
        fontSize: fontSizes.small,
        color: colors.black,
        opacity: 0.5,
        marginBottom: 5,
        fontFamily: 'Montserrat-Regular',
    },
    position: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    positionIcon: {
        width: 15,
        height: 15,
    },
    positionText: {
        fontSize: fontSizes.small,
        color: colors.black,
        marginLeft: 5,
        opacity: 0.5,
        fontFamily: 'Montserrat-Regular',
    },

});
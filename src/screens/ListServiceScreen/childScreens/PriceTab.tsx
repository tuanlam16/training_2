import { observer } from "mobx-react";
import { services } from "../../../constant/services";
import serviceStore from "../../../stores/serviceStore";
import { Service } from "../../../types/serviceTypes";
import { ContentContainer } from "./ContentComponent";


export const PriceTab = observer(() => {
   return (
        <ContentContainer service={serviceStore.getServicesByPrice} />
    );
})

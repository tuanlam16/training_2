import {observer} from 'mobx-react';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {Load} from '../../components/Load';
import {categories} from '../../constant/services';
import serviceStore from '../../stores/serviceStore';
import {colors} from '../../styles/colors';
import {fontSizes} from '../../styles/fonts';
import {categoryCar, Service} from '../../types/serviceTypes';
import {width} from '../../until/dimentions';
import {NearMeTab} from './childScreens/NearMeTab';
import {NewestTab} from './childScreens/NewestTab';
import {PopularTab} from './childScreens/PopularTab';
import {PriceTab} from './childScreens/PriceTab';

export const ListServiceScreen = observer(() => {
  const cateRef = React.useRef<FlatList>();

  const SearchBar = () => {
    return (
      <View style={styles.header}>
        <TouchableOpacity>
          <Image
            style={styles.back}
            source={require('../../assets/icons/back.png')}
          />
        </TouchableOpacity>
        <View style={styles.search}>
          <TextInput style={styles.input} placeholder="Rửa xe" 
          onChangeText={(text) => serviceStore.setSearchText(text)}
          />
          <Image
            style={styles.searchIcon}
            source={require('../../assets/icons/search.png')}
          />
          <TouchableOpacity style={styles.filter}>
            <Image
              style={styles.filterIcon}
              source={require('../../assets/icons/filter.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const IconPrice = observer(() => {
    return (
      <Image
        style={[
          styles.priceIcon,
          {
            transform: [
              {rotate: serviceStore.decendingPrice ? '180deg' : '0deg'},
            ],
          },
        ]}
        source={require('../../assets/icons/up.png')}
      />
    );
  });

  const TopTabNavigator = createMaterialTopTabNavigator(
    {
      NearMe: {
        screen: NearMeTab,
        navigationOptions: {
          tabBarLabel: 'Gần tôi',
          tabBarIcon: ({focused}) => (
            <Text style={focused ? styles.tabLabelActive : styles.tabLabel}>
              Gần tôi
            </Text>
          ),
        },
      },
      Newest: {
        screen: NewestTab,
        navigationOptions: {
          tabBarLabel: 'Mới nhất',
          tabBarIcon: ({focused}) => (
            <Text style={focused ? styles.tabLabelActive : styles.tabLabel}>
              Mới nhất
            </Text>
          ),
        },
      },
      Popular: {
        screen: PopularTab,
        navigationOptions: {
          tabBarLabel: 'Dùng nhiều',
          tabBarIcon: ({focused}) => (
            <Text style={focused ? styles.tabLabelActive : styles.tabLabel}>
              Dùng nhiều
            </Text>
          ),
        },
      },
      Price: {
        screen: PriceTab,
        navigationOptions: {
          tabBarLabel: 'Giá',
          tabBarIcon: ({focused}) => (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                width: width / 4,
              }}>
              <Text style={focused ? styles.tabLabelActive : styles.tabLabel}>
                Giá
              </Text>
              <IconPrice />
            </View>
          ),
          tabBarOnPress: ({navigation, defaultHandler}) => {
            if (navigation.isFocused()) {
              serviceStore.setDecendingPrice(!serviceStore.decendingPrice);
            }
            defaultHandler();
          },
        },
      },
    },
    {
      tabBarOptions: {
        activeTintColor: 'black',
        inactiveTintColor: 'gray',
        style: {
          backgroundColor: 'white',
          elevation: 0,
        },
        indicatorStyle: {
          backgroundColor: 'black',
          width: 20,
          marginLeft: width / 8 - 10,
        },
        showIcon: true,
        showLabel: false,
        iconStyle: {
          width: width / 4,
        },
      },
    },
  );

  const AppContainer = createAppContainer(TopTabNavigator);
  const Item = ({item}: {item: categoryCar}) => {
    return (
      <TouchableOpacity
        style={styles.cardCate}
        onPress={() => {
          serviceStore.setSelectedCategory(item);
          cateRef.current.scrollToIndex({
            animated: true,
            index: item.id - 1,
            viewPosition: 0.5,
          });
        }}>
        <Image style={styles.itemImageCate} source={item.image} />
        <Text style={styles.itemTextCate}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  const ListCategory = () => {
    return (
      <View style={styles.category}>
        <Text style={styles.titleText}>Danh mục</Text>
        <View style={styles.categoryList}>
          <FlatList
            ref={cateRef}
            data={categories}
            renderItem={({item}) => <Item item={item} />}
            keyExtractor={(item, index) => index.toString()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <SearchBar />
      <ListCategory />
      <AppContainer />
    </View>
  );
});
const styles = StyleSheet.create({
  // SearchBar
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  header: {
    width: width - 20,
    flexDirection: 'row',
    margin: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  back: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  search: {
    width: width - 60,
    marginRight: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  input: {
    width: '100%',
    height: 56,
    borderRadius: 13,
    fontSize: fontSizes.text,
    backgroundColor: colors.gray,
    paddingHorizontal: 60,
    fontFamily:'Montserrat-Regular'
  },
  searchIcon: {
    width: 20,
    height: 20,
    position: 'absolute',
    left: 20,
  },
  filter: {
    width: 20,
    height: 20,
    position: 'absolute',
    right: 20,
  },
  filterIcon: {
    width: 20,
    height: 20,
  },

  // ListCategory
  category: {
    width: width - 20,
    marginTop: 10,
  },
  titleText: {
    fontSize: fontSizes.title,
    fontFamily: 'Montserrat-Bold',
    color: colors.black,
    marginLeft: 15,
  },
  categoryList: {
    width: width - 20,
    marginTop: 10,
  },
  categoryItem: {
    width: 100,
    height: 100,
    borderRadius: 13,
    marginRight: 10,
  },
  cardCate: {
    width: 130,
    height: 130,
    borderRadius: 13,
    backgroundColor: colors.white,
    marginRight: 10,
    alignItems: 'center',
    overflow: 'hidden',
    marginLeft: 15,
    borderColor: colors.white,
    borderWidth: 1,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginBottom: 20,
  },
  itemImageCate: {
    width: 130,
    height: 90,
  },
  itemTextCate: {
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 10,
  },
  priceIcon: {
    width: 15,
    height: 15,
    position: 'absolute',
    right: 15,
    top: 2,
    zIndex: 1.2,
  },
  tabLabel: {
    fontSize: fontSizes.text,
    color: colors.black,
    width: width / 4,
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
  },
  tabLabelActive: {
    fontSize: fontSizes.text,
    color: colors.black,
    fontFamily: 'Montserrat-Bold',
    width: width / 4,
    textAlign: 'center',
  },
});

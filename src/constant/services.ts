import { categoryCar, Service } from "../types/serviceTypes";

export const categories : categoryCar[] = [
    {
        id: 1,
        name: 'Xe 4 chỗ',
        image: require('../assets/images/car4.jpg'),
    },
    {
        id: 2,
        name: 'Xe 7 chỗ',
        image: require('../assets/images/car7.jpeg'),
    },
    {
        id: 3,
        name: 'Xe 16 chỗ',
        image: require('../assets/images/car16.jpg'),
    },
    {
        id: 4,
        name: 'Xe 29 chỗ',
        image: require('../assets/images/car29.jpg'),
    }
];

export const services : Service[] = [
    {
        id: 1,
        name: 'Dầu nhớt',
        price: 700000,
        createdAt: '2020-10-10',
        quantity: 767361287,
        garage: {
            id: 1,
            name: 'Garage 1',
            image: require('../assets/images/garage1.jpg'),
            categoryId: [1, 2, 3, 4],
            rate: 4.5,
            verified: true,
            address: '123 Nguyễn Văn Cừ, Quận 5, TP.HCM',
            numberBranches: 2,
            distance: 1.5,
        }
    },
    {
        id: 2,
        name: 'Rửa xe 3 bước tặng mũ bảo hiểm',
        price: 600000,
        createdAt: '2020-10-12',
        quantity: 256,
        garage: {
            id: 2,
            name: 'Garage 2',
            image: require('../assets/images/garage2.jpg'),
            categoryId: [1],
            rate: 2.5,
            verified: true,
            address: '123 Nguyễn Văn Cừ, Quận 5, TP.HCM',
            numberBranches: 2,
            distance: 2,
        }
    },
    {
        id: 3,
        name: 'Combo rửa xe 3 bước + dầu nhớt',
        price: 800000,
        createdAt: '2020-10-09',
        quantity: 1090,
        garage: {
            id: 1,
            name: 'Garage 1',
            image: require('../assets/images/garage1.jpg'),
            categoryId: [1, 2, 3, 4],
            rate: 4.5,
            verified: true,
            address: '123 Nguyễn Văn Cừ, Quận 5, TP.HCM',
            numberBranches: 2,
            distance: 1.5,
        }
    },
    {
        id: 4,
        name: 'Combo rửa xe 5 bước + dầu nhớt',
        price: 1200000,
        createdAt: '2020-10-12',
        quantity: 56125,
        garage: {
            id: 1,
            name: 'Garage 1',
            image: require('../assets/images/garage1.jpg'),
            categoryId: [1, 2, 3, 4],
            rate: 4.5,
            verified: true,
            address: '123 Nguyễn Văn Cừ, Quận 5, TP.HCM',
            numberBranches: 2,
            distance: 1.5,
        }
    }
];
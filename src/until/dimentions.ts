import { Dimensions, StatusBar } from "react-native";

export const { width, height } = Dimensions.get("window");
export const statusBarHeight = StatusBar.currentHeight||0;
export const bottomBarHeight = Dimensions.get("screen").height - height- statusBarHeight;

import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { AppStack } from "./appStack";

const MainNav = () => {
    return (
        <NavigationContainer>
            <AppStack />
        </NavigationContainer>
    );
};
    
 export default MainNav;
    
import { createStackNavigator } from '@react-navigation/stack';
import { ListServiceScreen } from '../screens/ListServiceScreen/ListServiceScreen';

const Stack = createStackNavigator();

export const AppStack =() => {
  return (
    <Stack.Navigator
    screenOptions={
      {
        headerShown: false,
        cardStyle: { backgroundColor: 'white' },
      }
    }
    >
        <Stack.Screen name="ListCar" component={ListServiceScreen} />
    </Stack.Navigator>
  );
}
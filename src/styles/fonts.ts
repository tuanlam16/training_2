export const fontSizes = {
    title: 18,
    text: 15,
    small: 13,
}
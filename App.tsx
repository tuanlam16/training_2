import React from 'react';
import {LogBox, StatusBar, View} from 'react-native';
import MainNav from './src/navigation/mainNav';

export default function App() {
  LogBox.ignoreAllLogs();

  return (
    <>
      <StatusBar backgroundColor="black" />
        <MainNav />
    </>
  );
}
